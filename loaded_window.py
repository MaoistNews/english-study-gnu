from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import os,shutil

class Window(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master

# initialize tkinter
root = Tk()
app = Window(root)
f=open('setting.txt','r');loaded=f.read();f.close()

# initialize logging file
if not os.path.exists('logging'):os.makedirs('logging')
if not os.path.exists('logging/%s.txt'%loaded):
    f=open('dictionaries/%s.txt'%loaded,'r');lo=[[a.split('::')[0],a.split('::')[1],a.split('::')[2],eval(a.split('::')[3]),0,0,0,float(0)]for a in f.read().split('\n')if a];f.close()
    f=open('logging/%s.txt'%loaded,'w+');f.write(repr(lo));f.close()
else:f=open('logging/%s.txt'%loaded,'r');lo=eval(f.read());f.close()

# set window title and icon
root.wm_title("英语单词认识、拼写、选择、例句、综合、听力、构词法训练器 - %s"%loaded)
icon=PhotoImage(file='icon.png')
root.tk.call('wm','iconphoto',root._w,icon)

# set window size and position
w,h=1000,600
root.geometry("%dx%d+%d+%d"%(w,h,(root.winfo_screenwidth()-w)/2,(root.winfo_screenheight()-h)/2))

# set status of gird positions
Grid.columnconfigure(root,0,weight=1)
Grid.rowconfigure(root,1,weight=1)

# give the list of dictionaries
ti=Label(root,text="%s列表"%loaded,font=("song ti",24))
ti.grid(row=0,column=0,columnspan=9,sticky="NSEW")
style_head = ttk.Style()
style_head.configure("Treeview",font=("song ti", 10))
style_head.configure("Treeview.Heading",font=("song ti", 15))
ta=ttk.Treeview(root)
sc=Scrollbar(root,command=ta.yview)
sc.grid(row=1,column=10,sticky='NSEW')
sc2=Scrollbar(root,orient='horizontal',command=ta.xview)
sc2.grid(row=2,column=0,columnspan=9,sticky='NSEW')
ta.configure(yscrollcommand=sc.set,xscrollcommand=sc2.set)
ta['columns']=('ids','words','phonetics','meanings','examples','test_num','right_num','error_num','right_rate')
texts=['ID','单词','音标','语义','例句','测试次数','正确次数','错误次数','正确率']
ta.column("#0",width=0,stretch=NO)
for a in ta['columns']:ta.column(a,width=80)
ta.heading("#0",text="")
for a in range(len(ta['columns'])):ta.heading(ta['columns'][a],text=texts[a])
ta.grid(row=1,column=0,columnspan=9,sticky='NSEW')
los=[]
for a in range(len(lo)):
    los.append(lo[a])
    l=[a+1]
    for b in range(len(los[a])):l.append(los[a][b])
    los[a][3]=' § '.join([los[a][3][0][0],los[a][3][0][2],los[a][3][0][1]])
    los[a][4],los[a][5],los[a][6],los[a][7]=str(los[a][4]),str(los[a][5]),str(los[a][6]),'%d'%(los[a][7]*100)
    los[a].insert(0,str(a+1))
    g=los[a][3]
    los[a][3]=los[a][2]
    los[a][2]=g
    ta.insert(parent='',index='end',iid=a,text='',values=tuple(los[a]))

# give a button for confirm
def init_dictionary():
    se=ta.selection()
    lse=len(se)
    if not os.path.exists('learn_words_list'):os.makedirs('learn_words_list')
    if lse==0:
        shutil.copyfile('logging/%s.txt'%loaded,'learn_words_list/%s.txt'%loaded)
    else:
        if os.path.exists('learn_words_list/%s.txt'%loaded):
            f=open('dictionaries/%s.txt'%loaded,'r');lo=[[a.split('::')[0],a.split('::')[1],a.split('::')[2],eval(a.split('::')[3]),0,0,0,float(0)]for a in f.read().split('\n')if a];f.close()
            loa=[lo[int(a)]for a in se]
            f=open('learn_words_list/%s.txt'%loaded,'r');lo2=eval(f.read());f.close()
            if not loa==lo2:
                if messagebox.askokcancel("更新", "是否更新你选择的词汇？"):f=open('learn_words_list/%s.txt'%loaded,'w+');f.write(repr(loa));f.close()
        else:
            f=open('dictionaries/%s.txt'%loaded,'r');lo=[[a.split('::')[0],a.split('::')[1],a.split('::')[2],eval(a.split('::')[3]),0,0,0,float(0)]for a in f.read().split('\n')if a];f.close()
            loa=[lo[int(a)]for a in se]
            f=open('learn_words_list/%s.txt'%loaded,'w+');f.write(repr(loa));f.close()
def print_spell():print('欢迎开始拼写');init_dictionary();os.system('pyth spell_window.py')
def print_chinese_to_english():print('欢迎开始英译汉');init_dictionary();os.system('pyth select_chinese_window.py')
def print_english_to_chinese():print('欢迎开始汉译英');init_dictionary();os.system('pyth select_english_window.py')
def print_sound():print('欢迎开始听力');init_dictionary();os.system('pyth sound_window.py')
def print_sentence():print('欢迎开始审判');init_dictionary();os.system('pyth sentence_window.py')
bu0=Button(root,text='认识',font=("song ti",24));bu0.grid(row=3,column=0,sticky='NSEW')
bu1=Button(root,text='拼写',command=print_spell,font=("song ti",24));bu1.grid(row=3,column=1,sticky="NSEW")
bu2=Button(root,text='英译汉',font=("song ti",24),command=print_chinese_to_english);bu2.grid(row=3,column=2,sticky="NSEW")
bu3=Button(root,text='汉译英',font=("song ti",24),command=print_english_to_chinese);bu3.grid(row=3,column=3,sticky="NSEW")
bu4=Button(root,text='例句',font=("song ti",24),command=print_sentence);bu4.grid(row=3,column=4,sticky="NSEW")
bu5=Button(root,text='综合',font=("song ti",24));bu5.grid(row=3,column=5,sticky="NSEW")
bu6=Button(root,text='听力',font=("song ti",24),command=print_sound);bu6.grid(row=3,column=6,sticky="NSEW")
bu7=Button(root,text='构词法',font=("song ti",24));bu7.grid(row=3,column=7,sticky="NSEW")

# show window
root.mainloop()

