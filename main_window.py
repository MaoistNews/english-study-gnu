from tkinter import *
import os

class Window(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master

# initialize tkinter
root = Tk()
app = Window(root)

# set window title and icon
root.wm_title("英语单词认识、拼写、选择、例句、综合、听力、构词法训练器")
icon=PhotoImage(file='icon.png')
root.tk.call('wm','iconphoto',root._w,icon)

# set window size and position
w,h=800,600
root.geometry("%dx%d+%d+%d"%(w,h,(root.winfo_screenwidth()-w)/2,(root.winfo_screenheight()-h)/2))

# set status of gird positions
Grid.columnconfigure(root,0,weight=1)
Grid.rowconfigure(root,1,weight=1)

# give the list of dictionaries
ti=Label(root,text="请选择要学习的词典",font=("song ti",24))
ti.grid(row=0,column=0,sticky="NSEW")
lb=Listbox(root,font=("song ti",24))
lb.grid(row=1,column=0,sticky="NSEW")
ln=['大学四级词汇','大学四级词组','大学四级高频词汇','大学英语六级词汇','大学六级词组','英语专业四级词汇','英语专业八级词汇','GRE词汇','《自由软件，自由社会》']
for a in ln:lb.insert(END,a)

# give a button for confirm
def printSelected():
    if lb.get(ANCHOR)=='大学四级词汇':print('欢迎开始学习');f=open('setting.txt','w+');f.write('大学四级词汇');f.close();os.system('pyth loaded_window.py')
    if lb.get(ANCHOR)=='英语专业八级词汇':print('欢迎开始学习');f=open('setting.txt','w+');f.write('英语专业八级词汇');f.close();os.system('pyth loaded_window.py')
bu0=Button(root,text='确认进入学习',command=printSelected,font=("song ti",24))
bu0.grid(row=2,column=0,sticky="NSEW")
def check():os.system('pyth statis_window.py')
bu1=Button(root,text='查看统计数据',command=check,font=("song ti",24))
bu1.grid(row=3,column=0,sticky="NSEW")

'''
# lock window border
#root.resizable(width=False, height=False)
#Grid.rowconfigure(root,0,weight=1)
#ti.place(relx=0.5,anchor='n',y=8)
#ti.pack(fill=BOTH,expand=True,side=TOP)
#lb.place(relx=0.5,anchor='n',y=48,width=800,height=600-48-48)
#lb.pack(fill=BOTH,expand=True,side=TOP)
#bu0.place(relx=0.5,anchor='n',y=48+600-48-48)
#bu0.pack(fill=BOTH,expand=True,side=TOP)
'''

# show window
root.mainloop()
